<style>
	.overlay
	{
		position:fixed;
		top:-1000;
		left:0;
		height:0%;
		width:100%;
		display: table;
		
		opacity: 0;
		background-color: #efeff1;
		
		z-index:999;
		
		overflow: hidden; 
		transition: 0.75s;	
	}
	
	.overlay .overlaycontainer
	{
		display: table-cell;
		vertical-align: middle;
		text-align:center;
		width:100%;
		opacity:0;
		transition: 0.5s;
	}
	
	.overlay.show
	{
		top:0;
		opacity: 0.95;
		height:100%;
	}
	
	.overlay.show .overlaycontainer
	{
		opacity:1;
	}
/*
.select {
    border: 1px solid #e3e2ea;
    width: 100%;
	padding:10px;
    border-radius: 8px;
    overflow: hidden;
    background: #fff url("assets/img/select-down.png") no-repeat 95% 50%;
}

.select select {
    padding: 5px 8px;
    width: 100%;
    border: none;
    box-shadow: none;
    background: transparent;
    background-image: none;
    -webkit-appearance: none;
}

.select select:focus {
    outline: none;
}
*/

/*the container must be positioned relative:*/
.select {
  position: relative;
 
 
}

.select select {
  display: none; /*hide original SELECT element:*/
}

.select-selected {
 /* background-color: DodgerBlue;*/
 background:#fff;
 border:1px solid #e3e2ea;
 border-radius: 8px;
 color:#212a3d;
}

/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 18px;
  right: 10px;
  width: 11px;
  height: 7px;
  background: #fff url("assets/img/select-down.png") no-repeat;
}

/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
	background: #fff url("assets/img/select-up.png") no-repeat;
}

/*style the items (options), including the selected item:*/
.select-items div, .select-selected {
  color:#212a3d;
  background:#fff;
  padding: 8px 16px;
  border: 1px solid #e3e2ea;
  cursor: pointer;
  user-select: none;
}

/*style items (options):*/
.select-items {
  position: absolute;

  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}

/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}

.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.5);
}

</style>

<div id="redirectOverlay" class="overlay">
	<div class="overlaycontainer centered">

		<img src="assets/img/logo-pink.svg" alt="logo" width="49"/>
		
		<br/><br/>
		
		<div class="frame">
		<div class="card">
		
		<div class="row">
		<div class="col">
		<h1>Je verlaat nu<br/>www.schluss.app</h1>
		
		<hr />
		
		<p>En gaat naar<br/>
		<img src="assets/img/arrow-pink-right.svg" alt="arrow right"/> <span id="redirecturlname" style="color:#fb637e;font-weight:bold;"></span>
		</p>
		
		</div>
		</div>
		
		</div>
		</div>

	</div>
</div>

<header>
	<div class="frame">
		<div class="logo" style="float:left;">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49" />
		</div>
		
		<div style="float:right;">
			<a href="#/">Afbreken &nbsp; <img src="assets/img/abort.svg" alt="abort" style="vertical-align:middle;" /></a>
		</div>
	</div> 
</header>

<main>
	<div class="frame">
		
		<div class="card">
		
			<div class="row">
			
				<div class="col quarter">
		
					<p>
					<img src="assets/img/idin-logo.svg" alt="iDIN logo"/>
					</p>
				</div>
			</div>
			
			<div class="row">
				<div class="col">
				
					<h1 class="left">Bij welke bank zit je?</h1>
		
					<p>iDIN is een manier om je identiteit bij je bank door te geven aan Schluss.</p>
		
					<br/>
		
					<div class="select" id="bankSelectContainer">
					<select id="bankSelect">
						<option value="0" readonly>Kies uw bank...</option>
					<!--	<option value="1ing">ING Bank</option>
						<option value="2rabo">Rabobank</option>
						<option value="3other">Andere bank</option> -->
					</select>
					</div>
		
		
		
				</div>
			</div>
		</div>
	</div>
</main>	

<footer></footer>