<script>
		$('#hamburger').click(function(){
			$('#mainnav').toggleClass('show');
		});
</script>

<header>

	<div class="frame">
	
		<div class="logo" style="float:left;">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49" />
		</div>
		
		<nav id="mainnav">
		
			<div id="slider">
			
				<div class="logo">
					<a href="#/"><img src="assets/img/logo-white.svg" alt="logo" width="49" /></a>
				</div>			
			
				<ul class="">
					<li><a href="#/">Dashboard</a></li>
					<li><a href="#/data">Gegevens</a></li>
					<li><a href="#/shares">Inzages</a></li>
				</ul>
				
				<ul class="smaller">
					<li><a href="https://www.schluss.org" target="_blank"><img src="assets/img/support.svg" alt="logo" width="20"/> Support</a></li>
					<li><a href="#/logout"><img src="assets/img/logout.svg" alt="logout" width="20" /> Uitloggen</a></li>
				</ul>
			</div>
			
			
			<div id="hamburger">
				<span></span>
				<span></span>
				<span></span>		
			</div>
			
		
		</nav>
	
	</div>

</header>

<main>
	<div class="frame">
		
		<div class="card">
		
			<div class="row">
			
				<div class="col quarter">
		
					<p>
					<img src="assets/img/idin-logo.svg" alt="iDIN logo"/>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
				
					<h1 class="left">Plaats je identiteit in de kluis!</h1>
					
					<h4>Wat is een identiteit?</h4>
					
					<p>
					
					Je anonieme kluis wordt geladen met identiteit, Schluss kent deze niet.</p>
					
					<br/>
					
					<h4>Wat is iDIN?</h4>
					
					<p>
					
					iDIN is een manier om je identiteit bij de bank door te geven aan Schluss.</p>
					
				</div>
			</div>
			
			<div class="row">
				<div class="col centered">			
					<p><a href="#/idin/select-bank" class="button" id="startprocess">Start traject</a>
					
					<br/>
					<br/>
					
					<a href="#/">Terug</a>
					
					</p>
					
				</div>
			</div>
		</div>
	</div>
</main>	

<!--
<footer>
	<div class="centered">
		<p><a href="#/idin/select-bank" class="button">Plaats nu je gegevens in je kluis</a></p>
	</div>
</footer>
-->
