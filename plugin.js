// IDIN config params -------------------
const idinApiUrl = 'https://idintest.schluss.app/api.php/api';
var idinApiConfigUrl = ''; 
var idinScope='';
const config = {
	viewpath : './app_modules/node_modules/@schluss/plugin-idin/',
};

pubsub.subscribe('onRegisterSuccess', function(){
		var notifications = [];
		notifications.push({
			id : 'idin_tip',	
			type : 'tip', 
			title : '',
			text : '<h3><span class="highlight"><b>Tip:</b></span> Plaats je identiteit in de kluis!</h3>',
			action : '#/idin/about'
			});
		 db.set('app', 'notifications', notifications);
});
	
module.exports={
	init:function(context){
		idinScope=context.request.scope;
		idinApiConfigUrl=context.request.apiconfigurl;
		this.getidin_select_bank(context);
		this.idinReturnRequestSubscribe(context);
	},
	getidin_select_bank : async function(context) {
		let e = context.sammy;
		let tools=context.tools;
		//Rendering about view
		e.partial(config.viewpath+'views/about.tpl', null, function(t){
			tools.setTheme('grey');
			tools.setLayout('wizard');
			document.getElementById('startprocess').onclick = () => {
				e.partial(config.viewpath+'views/select-bank.tpl', null, function(t){	
					tools.setTheme('grey');
					tools.setLayout('app');	
					// get banks
					tools.xhr.getJSON(idinApiUrl + '/issuers').then(function(result){
						// fill select list options
						for (var i = 0; i < result.data[0].issuers.length; i++){	
							var bank = result.data[0].issuers[i];
							var opt = $('<option value="'+ bank.bank_id +'">'+ bank.bank_name +'</option>');
							opt.data('image', bank.bank_name);
							$('#bankSelect').append(opt)
						}
						
						// style selectbox
						tools.smartSelect('bankSelectContainer');					
					});
					
					// when bank selected
					$('#bankSelect').on('change', async function(){
						if (this.value == 0)
							return;	
						
						// screen wait helper: start the time
						tools.timer.start();	

						// show overlay that user s beeing redirect externally
						$('#redirectOverlay').addClass('show');

						$('#redirecturlname').text(tools.shorten(idinApiUrl, 30));
						
						// get config.json
						let config = await $.getJSON(idinApiConfigUrl);
						
						var key = tools.uuidv4();
						
						var rUrl = config.endpoints.connect;
						rUrl = config.endpoints.connect.replace('[token]', encodeURIComponent(key));
						rUrl = rUrl.replace('[returnurl]', 'android-app://lk.yukon.hybrid_schluss.app/https/idintest.schluss.app/returnrequest/' + encodeURIComponent(key));	
						rUrl = rUrl.replace('[settingsurl]', encodeURIComponent('https://beta.schluss.app/config.json'));
						rUrl = rUrl.replace('[scope]', encodeURIComponent(idinScope));
						rUrl = rUrl.replace('[bankid]', encodeURIComponent(this.value));
						
						console.log('CONNECT URL : '+rUrl);
						window.location.href = rUrl;							
					});
				});
			};
		});
	},
	idinReturnRequestSubscribe :async function(context) {
		let pgpKeyPair = context.getKeyPair();					
		tools.log("Get Key pair " + pgpKeyPair);

		universalLinks.subscribe('idinReturnRequest', function (eventData) {
			let hashKey=eventData.path.replace('/returnrequest/','');
			let retriveURL = "https://idintest.schluss.app/api.php/api/retrieve";
			console.log('return request hash  : '+encodeURIComponent(hashKey));
			$.post(retriveURL, {token :  encodeURIComponent(hashKey), publickey : pgpKeyPair.public}).then(function(res){
				context.store(res, context.request.id, 'plugin-idin').then(() =>{
					context.redirect(context.request.id);
				});
			});	
		}); 
	}
};